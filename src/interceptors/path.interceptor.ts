import {
  injectable,
  Interceptor,
  InvocationContext,
  InvocationResult,
  Provider,
  ValueOrPromise,
} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Request, RestBindings} from '@loopback/rest';
import {Path} from '../models';
import {PathRepository} from '../repositories';

/**
 * This class will be bound to the application as an `Interceptor` during
 * `boot`
 */
@injectable({tags: {key: PathInterceptor.BINDING_KEY}})
export class PathInterceptor implements Provider<Interceptor> {
  static readonly BINDING_KEY = `interceptors.${PathInterceptor.name}`;

  constructor(
    @repository(PathRepository) private pathRepository: PathRepository,
  ) {}

  /**
   * This method is used by LoopBack context to produce an interceptor function
   * for the binding.
   *
   * @returns An interceptor function
   */
  value() {
    return this.intercept.bind(this);
  }

  /**
   * The logic to intercept an invocation
   * @param invocationCtx - Invocation context
   * @param next - A function to invoke next interceptor or the target method
   */
  async intercept(
    invocationCtx: InvocationContext,
    next: () => ValueOrPromise<InvocationResult>,
  ) {
    const request: Request = await invocationCtx.get(RestBindings.Http.REQUEST);
    const {url, method}: {url: string; method: string} = request;
    const path = await this.pathRepository.findOne({
      where: {path: url, method},
    });
    if (!path) {
      const newPath = new Path({
        path: url,
        method,
      });
      await this.pathRepository.create(newPath);
    } else {
      path.updatedAt = new Date();
      await this.pathRepository.updateById(path.id, path);
    }
    const result = await next();
    return result;
  }
}
