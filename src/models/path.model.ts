import {Entity, model, property} from '@loopback/repository';

@model()
export class Path extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  path: string;

  @property({
    type: 'string',
    required: true,
  })
  method: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt: Date;

  constructor(data?: Partial<Path>) {
    super(data);
  }
}

export interface PathRelations {
  // describe navigational properties here
}

export type PathWithRelations = Path & PathRelations;
