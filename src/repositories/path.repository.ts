import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Path, PathRelations} from '../models';

export class PathRepository extends DefaultCrudRepository<
  Path,
  typeof Path.prototype.id,
  PathRelations
> {
  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
  ) {
    super(Path, dataSource);
  }
}
