# ejercicioloopback4

Se desea crear un middleware para capturar los PATH de todas las peticiones en donde se aplique el middleware. ademas el middleware debe realizar la actualizacion del campo <updated, update_at, update_now, etc> en las tablas asociadas al endpoint.

## Entregable

El Interceptor bajo especificaciones del framework es encuentra ubucado en

```sh
/src/interceptor/path.interceptors.ts
```

- Se uso los modelos y migraciones nativas de loopback 4
- El interceptor modifica los campos de `updatead` del modelo `Path`, si el registro no existe lo crea en base al PATH visitado en la aplicación

## Clonar proyecto

```sh
git clone https://gitlab.com/giothcode/ejercicio-loopback-4.git
```

## Instalar dependencias

```sh
npm install
```

## Levantar base de datos PostgreSQL

```sh
npm run docker:run:db
```

Es necesario la existencia de una DB para poder ejecutar las migraciones

## Ejecutar migraciones

```sh
npm run migrate
```

## Ejecutar la aplicación

```sh
npm start
```

Abra http://127.0.0.1:3000 en su navegador.

## Reconstruir el proyecto

```sh
npm run build
```

Para forzar una compilación completa limpiando los artefactos almacenados en caché:

```sh
npm run rebuild
```

## Fix code style and formatting issues

```sh
npm run lint
```

To automatically fix such issues:

```sh
npm run lint:fix
```

## Other useful commands

- `npm run migrate`: Migrate database schemas for models
- `npm run openapi-spec`: Generate OpenAPI spec into a file
- `npm run docker:build`: Build a Docker image for this application
- `npm run docker:run`: Run this application inside a Docker container

## Tests

```sh
npm test
```

## What's next

Please check out [LoopBack 4 documentation](https://loopback.io/doc/en/lb4/) to
understand how you can continue to add features to this application.

[![LoopBack](<https://github.com/loopbackio/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png>)](http://loopback.io/)
